gulp-add-missing-post-images
============================

> A *gulp* plugin for creating an image based on a property in a post's metadata.

## Setup and usage

Install `gulp-add-missing-postimages` using `npm`:

```sh
npm i gulp-add-missing-postimages
```

In your `gulpfile.js`:

```js
var gulp = require('gulp'),
    addMissingPostImages = require('gulp-add-missing-post-images');

gulp.task('default', function() {
  return gulp.src('./src/**.*')
    .pipe(addMissingPostImages({
        path: 'data.relativePath',
        title: 'page.title',
        image: 'page.featuredImage.url',
        prefix: 'post-',
        cache: 'cache'
    }))
    .pipe(gulp.dest('./dest'));
});
```

A common use for this plugin is to create post-based icons (using [jdenticon](jdenticon.com)) based on a consistent source such as the relative path in the system or the title.

## Options

options.prefix = options.prefix || '';

### background

*number*

Default: `0x00000000`

The hex of the background color. By default, this is transparent.

### cache

*string*

Default: `undefined`

The path to cache the resulting images. If this is undefined, then no cache will be used. If one is there, then if the image exists there, it will be used instead of generating a new one. Likewise, all created images will be placed in the cache.

### columns

*number*

Default: `1`

The number of columns to repeat the image.

### image

*string*

Default: `data.image`

The name of the property (which may have dots) to both determine if the post already has an image (if so, no image will be generated) and to store the resulting relative path of the image once one is created.

### padding

*number*

Default: `0`

The number of pixels to place around the tile image (or all the images of multiples are included).

### path

*string|string[]*

Default: `path`

The name of the property in the file that contains the path of the item. This is used when the path changes relative to other elements. This may go into nested items by using dots (e.g., `data.paths.relative`). If multiple path objects are given, then each one will be mapped.

### prefix

*string*

Default: `""`

The prefix to place before the generated file name.

### rows

*number*

Default: `1`

The number of rows of duplicated images should be included.

### size

*number*

Default: `256`

The number of pixels for the resulting square image. If multiples icons are included, then this will only be the individual tiles.

### spacing

*number*

Default: `0`

The number of pixels between each image of more than one is included.

### title

*string*

Default: `data.title`

The name of the property (which may have dots) to find the source text to generate the image from.

### extensions

*string[]*

Deafult: `['.html']`

A list of extensions to move into their own directories. Files that don't match the given extension will be ignored.
