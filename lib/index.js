var crypto = require('crypto');
var dotted = require('dotted');
var fs = require('fs');
var jdenticon = require('jdenticon');
var Jimp = require('jimp');
var mkdirp = require('mkdirp');
var path = require('path');
var promisify = require('promisify-any');
var svg2png = require('svg2png');
var through = require('through2');
var File = require('vinyl');
var Promise = require('any-promise');

module.exports = function(options) {
    // Normalize the options.
    options = options || {};
    options.path = options.path || ['path'];
    options.path = Array.isArray(options.path) ? options.path : [options.path];
    options.title = options.title || 'data.title';
    options.image = options.image || 'data.image';
    options.size = options.size || 256;
    options.prefix = options.prefix || '';
    options.background =
        options.background === undefined ? 0x00000000 : options.background;
    options.padding = options.padding || 0;
    options.spacing = options.spacing || 0;
    options.rows = options.rows || 1;
    options.columns = options.columns || 1;

    // Make sure the cache directory exists, if we need it.
    if (options.cache) {
        mkdirp.sync(options.cache);
    }

    // Operations for building image.
    function createImage(img) {
        // Create the canvas we'll be drawing on.
        var bgColor = options.background;
        var size = options.size;
        var bWidth = options.padding * 2
            + options.spacing * (options.columns - 1)
            + size * options.columns;
        var bHeight = options.padding * 2
            + options.spacing * (options.rows - 1)
            + size * options.rows;
        var back = new Jimp(bWidth, bHeight, bgColor);

        // Loop through the rows and columns and place each image.
        var rowOffset = options.padding;

        for (var row = 0; row < options.rows; row++) {
            var colOffset = options.padding;

            for (var col = 0; col < options.columns; col++) {
                // Figure out which image we'll be blitting.
                var blit = img;

                // Place it in the right place.
                back = back
                    .composite(blit, colOffset, rowOffset);

                // Jump forward.
                colOffset += size + options.spacing;
            }

            rowOffset += size + options.spacing;
        }

        // Return the resulting image.
        return back;
    }

    // Set up the pipe that processes the file names.
    var setPipe = through.obj(
        function(file, encoding, callback) {
            // Get the URL and see if it exists, if it does, we don't have to
            // process anything.
            var image = dotted.getNested(file, options.image);

            if (image) {
                return callback(null, file);
            }

            // We are missing the image, so generated it from the value. To do
            // that, we first need to get the hash of the title. We do this a
            // number of times because we handle doing side-by-side images for
            // places like Facebook which has a 1.91:1 ratio for shared links.
            var title = dotted.getNested(file, options.title, true);

            if (!title || typeof (title) !== 'string') {
                return callback(null, file);
            }

            var hash = crypto.createHash('md5').update(title).digest('hex');

            // Because generating icons is computationally expensive, if we
            // have a cache provided, then check that first.
            var pngBaseName = options.prefix + hash + '.png';
            var pngPromise = undefined;

            if (options.cache) {
                var pngCacheFileName = path.join(options.cache, pngBaseName);

                if (fs.existsSync(pngCacheFileName)) {
                    var png = fs.readFileSync(pngCacheFileName);

                    pngPromise = Promise.resolve(png);
                }
            }

            if (!pngPromise) {
                // Get the SVG that represents this hash.
                var icon = jdenticon.toSvg(hash, options.size);

                // Convert the SVG to a PNG icon and insert it into the stream.
                pngPromise = Promise
                    .resolve(icon)
                    .then(svg2png)
                    .then(function (pngBuffer) {
						return Jimp.read(pngBuffer);
					})
                    .then(function (img) {
                        // The resulting SVG to PNG image has a transparent
                        // background which places like Facebook doesn't handle
                        // well. To get around that, we create a new solid
                        // background and composite the image on top of it to
                        // create a solid, non-transparent image.
                        return promisify(function (cb) {
                            var back = createImage(img);
                            return back.getBuffer(Jimp.MIME_PNG, cb);
                        })();
                    });
            }

			// Set up the PNG generation promise.
			var that = this;

            return pngPromise
                .then(function (png) {
                    // Create the virtual PNG image along with all the paths.
                    var pngFile = new File({
                        cwd: file.cwd,
                        base: file.base,
                        contents: png
                    });

                    for (var pathIndex = 0;
                         pathIndex < options.path.length;
                         pathIndex++) {
                         // Figure out the relative path we need.
                         var sourcePath = options.path[pathIndex];
                         var filePath = dotted.getNested(file, sourcePath);

                         // If we have a path, then set the value.
                         if (filePath) {
                             dotted.setNested(
                                 pngFile,
                                 sourcePath,
                                 path.join(path.dirname(filePath), pngBaseName),
                                 { ensure: true });
                        }
                    }

                    // Add the png file into the stream.
                    that.push(pngFile);

                    // If we are caching the results, then store it there
					// for later.
                    if (options.cache) {
                        var pngCacheFileName =
							path.join(options.cache, pngBaseName);

                        fs.writeFileSync(pngCacheFileName, png);
                    }

                    // Update the source file to have the path and finish up.
                    dotted.setNested(
						file,
						options.image,
						pngBaseName,
						{ ensure:true });
                    callback(null, file);
                });
        });

    return setPipe;
};
