'use strict';

var expect = require('expect');
var streamify = require('stream-array');
var plugin = require('../lib/index');
var File = require('vinyl');

describe('gulp-add-missing-post-images', function() {
    it('missing image', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: {
                title: "Title"
            },
            path: 'bob.html'
        });

        // Run the plugin through the stream and look at the output.
        var files = [];

        streamify([fakeFile])
            .pipe(plugin({}))
            .on('data', function(file) { files.push(file); })
            .on('end', function() {
                var image = files[0];
                var html = files[1];

                expect(image.path)
                    .toEqual(
                        'b78a3223503896721cca1303f776159b.png',
                        'image.path was not expected');

                expect(html.data.image)
                    .toEqual(
                        'b78a3223503896721cca1303f776159b.png',
                        'html.data.url was not expected: ' + html.data.image);

                done();
            });
    });

    it('missing image with path', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: {
                title: "Title"
            },
            path: 'a/b/bob.html'
        });

        // Run the plugin through the stream and look at the output.
        var files = [];

        streamify([fakeFile])
            .pipe(plugin({}))
            .on('data', function(file) { files.push(file); })
            .on('end', function() {
                var image = files[0];
                var html = files[1];

                expect(image.path)
                    .toEqual(
                        'a/b/b78a3223503896721cca1303f776159b.png',
                        'image.path was not expected');

                expect(html.data.image)
                    .toEqual(
                        'b78a3223503896721cca1303f776159b.png',
                        'html.data.url was not expected: ' + html.data.image);

                done();
            });
    });

    it('provided image', function(done) {
        // Create a fake file that simulates `matter` being called on it.
        var fakeFile = new File({
            data: {
                title: "Title",
                image: "bob"
            },
            path: 'bob.html'
        });

        // Run the plugin through the stream and look at the output.
        var files = [];

        streamify([fakeFile])
            .pipe(plugin({}))
            .on('data', function(file) { files.push(file); })
            .on('end', function() {
                var html = files[0];

                expect(html.data.image)
                    .toEqual(
                        'bob',
                        'html.data.url was not expected: ' + html.data.image);

                done();
            });
    });
});
